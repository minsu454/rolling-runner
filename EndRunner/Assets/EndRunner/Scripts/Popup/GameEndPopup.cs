﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndPopup : BasePopup
{
    public void End() {
        SoundManager.instance.PlaySFX(SfxType.Button);
        Application.Quit();
    }
}
