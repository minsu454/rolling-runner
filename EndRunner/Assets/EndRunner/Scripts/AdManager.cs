﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdManager : MonoBehaviour
{
    public static AdManager instance;

    private string aosAppID = "ca-app-pub-7551089498596269~5777092282";
    private string iosAppID = "";
    
    private string aosRewardAdID = "ca-app-pub-7551089498596269/2713204429";
    private string iosRewardAdID = "";
    private string testRewardAdID = "ca-app-pub-3940256099942544/5224354917";

    private RewardBasedVideoAd rewardBasedVideo;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void Init()
    {
        GameObject obj = new GameObject("AdManager");
        instance = obj.AddComponent<AdManager>();
        DontDestroyOnLoad(obj);
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(3);

#if UNITY_ANDROID
        string appId = aosAppID;
#elif UNITY_IPHONE
            string appId = "";
#else
            string appId = "unexpected_platform";
#endif
        TitleManager.instance.ShowTestLabel("Init");
        MobileAds.Initialize(appId);

        this.rewardBasedVideo = RewardBasedVideoAd.Instance;

        // Called when an ad request has successfully loaded.
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

        RequestRewardBasedVideo();
    }

    private void RequestRewardBasedVideo()
    {
#if UNITY_ANDROID
        string adUnitId = aosRewardAdID;
#elif UNITY_IPHONE
            string adUnitId = testRewardAdID;
#else
            string adUnitId = "unexpected_platform";
#endif
        //TitleManager.instance.ShowTestLabel("Request");
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, adUnitId);
    }

    public void WatchAd()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
        }
        else
        {
            //예외 팝업
            BasePopup basePopup = PopupContainer.CreatePopup(PopupType.ConfirmationPopup);
            ((ConfirmationPopup)basePopup).SetCallback(() =>
            {
                PopupContainer.GetActivatedPopup(2).Close();
                GameManager.instance.GameOver();
            });
            ((ConfirmationPopup)basePopup).SetText(DataService.Instance.GetText(23));
            basePopup.Init();
        }
    }

    #region Events
    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoLoaded event received");
        //TitleManager.instance.ShowTestLabel("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //TitleManager.instance.ShowTestLabel("HandleRewardBasedVideoFailedToLoad event received with message: "
        //                     + args.Message);
        Debug.Log(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);

        RequestRewardBasedVideo();
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoClosed event received");
        RequestRewardBasedVideo();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        //string type = args.Type;
        //double amount = args.Amount;
        //Debug.Log(
        //    "HandleRewardBasedVideoRewarded event received for "
        //                + amount.ToString() + " " + type);

        //부활 코드
        GameManager.instance.Bomb();
        GameManager.instance.ManaReset();
        GameManager.instance.ReSet();
        PlayerManager.instance.hatSprite.gameObject.SetActive(true);
        PlayerManager.instance.eyeSprite.gameObject.SetActive(true);
        PlayerManager.instance.tieSprite.gameObject.SetActive(true);
        PopupContainer.GetActivatedPopup().Close();
        PlayerManager.instance.gameObject.SetActive(true);
        PlayerManager.instance.restartPaticle.SetActive(true);
        SoundManager.instance.SetBGM(1f);
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoLeftApplication event received");
    }
    #endregion
}
