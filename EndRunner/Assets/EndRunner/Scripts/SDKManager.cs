﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class SDKManager : MonoBehaviour
{
    public static bool isLoginSuccess = false;

    // Start is called before the first frame update
    void Start()
    {
        if (isLoginSuccess)
        {
            return;
        }
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success, string error) =>
        {
            if (success)
            {
                isLoginSuccess = true;
            }
        });
    }
}
